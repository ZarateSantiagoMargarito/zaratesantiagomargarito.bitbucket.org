$(document).ready(function(){
	var collection = new Array();
	var pointMarker = new Array();

	$('#search').click(function(){
		query=$('#query').val();
		getUStream();
        getEvents();		
		getAlbums();
		getMerchandise();
		showUStream();
	});

	function getUStream(){
		var URL="http://api.ustream.tv/json/channel/all/search/title:like:"+query+"?key=0CCE0C3A3B6C885DBD2EBA5484CF97AB&limit=10"
        $.ajax({
        	dataType: 'jsonp',
			method:'GET',
			url:URL,
			success:showUStream			
		})
	}

	function getAlbums(){
		var URL="http://ws.audioscrobbler.com/2.0/?method=artist.getTopAlbums&artist="+query+"&limit=10&api_key=b25b959554ed76058ac220b7b2e0a026&format=json"
		$.ajax({
			method:'GET',
			url:URL,
			success:showAlbums			
		})
	}	
	function getEvents(){
		var URL= "http://api.bandsintown.com/artists/"+query+"/events.json?api_version=2.0&app_id=LucySeven"
		console.log("Obteniendo Datos de bandsInTaown");
		$.ajax({
			dataType: 'jsonp',
			method:'GET',
			url:URL,
			success:showEvent			
		})
	}
	function getMerchandise(){
		var URL="https://api.mercadolibre.com/sites/MLM/search?category=MLA1168&q="+query
		$.ajax({
			method:'GET',
			url:URL,
			success:showMerchandise			
		})
	}
	function showUStream(data){	
		$("#ustream").empty();	
		for(var i=0; i<data.length;i++){
			//console.log(data.results[i].thumbnail)
			$("#ustream").append(data[i].embedTag);
		}		
	}
	function showMerchandise(data){	
		$("#merchandise").empty();	
		for(var i=0; i<data.results.length;i++){
			//console.log(data.results[i].thumbnail)
			var tag='<div class="row"><div class="col-xs-4 col-sm-3"><img src="'+data.results[i].thumbnail+'" class="img-thumbnail"></div><div class="col-xs-8 "><strong>'+data.results[i].title+'</strong></div><div class="col-xs-6 ">$'+data.results[i].price+'</div><div class="col-xs-6 "><a href="'+data.results[i].permalink+'">Comprar</a></div></div><hr>'
			$("#merchandise").append(tag);
		}				
	}

	function showAlbums(data){	
		var json = JSON.stringify(data)
		var jsonP =json.replace(/#/g,'');
		var jsonV = jQuery.parseJSON(jsonP);
		//console.log(jsonV.topalbums.album[0].image[1].text)
		$("#albums").empty();	
		for(var i=0; i<data.topalbums.album.length; i++){
			var tag ='<div class="row"><div class="col-xs-4 col-sm-2"><img src="'+jsonV.topalbums.album[i].image[1].text+'" class="img-rounded"></div><div class="col-xs-8 "><strong>'+jsonV.topalbums.album[i].name+'</strong></div><div class="col-xs-6 ">Reproducciones: '+jsonV.topalbums.album[i].playcount+'</div><div class="col-xs-6 "><a href="'+jsonV.topalbums.album[i].url+'">Escuchar en Last.FM</a></div></div><br>'
			$("#albums").append(tag);
		}
	}
	
	function showEvent(data){
		$("#tbody_events").empty();
		eraseMarkers();
		console.log("Llenando arreglo");
  		for(var i=0; i<data.length; i++){
  			//console.log(data[i].venue.latitude+" - "+data[i].venue.longitude);
			$("#tbody_events").append('<tr><td>'+data[i].title+'</td><td>'+data[i].formatted_datetime+'</td></tr>');
  			collection[i] = new google.maps.LatLng(data[i].venue.latitude, data[i].venue.longitude);
  		}
		setPoint(collection);
	}	
	function setPoint(collection){	
		for(var i=0; i<collection.length; i++){
			pointMarker[i] = new google.maps.Marker({
			        position: collection[i],
			        map: map,
    				icon: 'icon.ico'
			});			
		}
	}
	function eraseMarkers(){
		//console.log(pointMarker.length);
		for(var i=0; i<pointMarker.length;i++){			
			pointMarker[i].setMap(null);
		}

	}
})